package org.nrg.xnat.utils;

import org.apache.log4j.Logger;
//import org.codehaus.jackson.JsonParseException;
import com.fasterxml.jackson.core.JsonParseException;
//import org.codehaus.jackson.map.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * Created by Michael Hileman on 2015/12/30.
 * Utility methods to create XSD for given NDAR data type.
 * For more info on NDAR, see https://ndar.nih.gov/ndar_data_dictionary.html
 */

public class NdarDatatypeImportUtils extends DatatypeCreator {
    static private org.apache.log4j.Logger logger = Logger.getLogger(NdarDatatypeImportUtils.class);

    public DatatypeCreator datatypeCreator = new DatatypeCreator();

    public NdarDatatypeImportUtils () {}

    public String apiUrl = "https://ndar.nih.gov/api/datadictionary/v2/";

    // TODO use a list of provided namespaces instead of hardcoding
    private String xsdHead =
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<xs:schema targetNamespace=\"http://nrg.wustl.edu/nda\"\n" +
            "    xmlns:nda=\"http://nrg.wustl.edu/nda\"\n" +
            // "    xmlns:hcp=\"http://nrg.wustl.edu/hcp\"\n" +
            "    xmlns:xnat=\"http://nrg.wustl.edu/xnat\"\n" +
            "    xmlns:xdat=\"http://nrg.wustl.edu/xdat\"\n" +
            "    xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"\n" +
            "    elementFormDefault=\"qualified\" " +
            "attributeFormDefault=\"unqualified\">\n" +
            "<xs:import namespace=\"http://nrg.wustl.edu/xnat\" " +
            "schemaLocation=\"../xnat/xnat.xsd\"/>\n\n" +
            "<xs:element name=\"%s\" type=\"nda:%s\"/>\n\n" +
            "    <xs:complexType name=\"%s\">\n" +
            "        <xs:complexContent>\n" +
            "            <xs:extension base=\"xnat:subjectAssessorData\">\n";

    private String xsdElement =
            "                <xs:element name=\"%s\" type=\"xs:%s\" minOccurs=\"0\" maxOccurs=\"1\">\n" +
            "                    <xs:annotation>\n" +
            "                        <xs:documentation>\n" +
            "                            %s\n" +
            "                        </xs:documentation>\n" +
            "                    </xs:annotation>\n" +
            "                </xs:element>\n";

    private String xsdFoot =
            "            </xs:extension>\n" +
            "        </xs:complexContent>\n" +
            "    </xs:complexType>\n" +
            "</xs:schema>";

    public ArrayList<Map<String, Object>> getJsonList(String url) {
        ArrayList<Map<String, Object>> dataTypes = null;
        URL _url = null;
        try {
            _url = new URL(url);
        } catch (MalformedURLException e) {
            logger.error(e.getMessage());
        }

        try {
            ObjectMapper mapper = new ObjectMapper();
            dataTypes = mapper.readValue(_url, ArrayList.class);
        } catch (JsonParseException e) {
            if (_url != null) {
                logger.error("Error parsing json from url " + _url.toString());
            }
            logger.error(e.getMessage());
        } catch (IOException e) {
            logger.error(e.getMessage());
        } catch (NullPointerException e) {
            logger.error(e);
        }

        return dataTypes;
    }

    private Map<String, Object> getJsonMap(String url) {
        Map<String, Object> dataType = null;
        URL _url = null;
        try {
            _url = new URL(url);
        } catch (MalformedURLException e) {
            logger.error(e.getMessage());
        }

        try {
            ObjectMapper mapper = new ObjectMapper();
            dataType = mapper.readValue(_url, Map.class);
        } catch (JsonParseException e) {
            if (_url != null) {
                logger.error("Error parsing json from url " + _url.toString());
            }
            logger.error(e.getMessage());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        return dataType;
    }

    public String createXsd(String ndarDataType) {
        // Get Json for NDAR data type param
        String url = this.apiUrl + "datastructure/" + ndarDataType;
        Map<String, Object> dataType = this.getJsonMap(url);

        // Fill in header parameters
        Object[] formatArgs = {ndarDataType, ndarDataType, ndarDataType};
        this.xsdHead = String.format(this.xsdHead, formatArgs);

        // Build body from data type elements
        String body = "";

        for (Map<String, Object> elem : (ArrayList<Map<String,Object>>)dataType.get("dataElements")) {
            if (elem.get("type").toString().toLowerCase().equals("guid"))
                continue;

            Object[] elementArgs = {elem.get("name"), elem.get("type").toString().toLowerCase(), elem.get("description")};
            String element = String.format(xsdElement, elementArgs);
            body += element;
        }

        return this.xsdHead + body + this.xsdFoot;
    }

    public void generateDatatype(String datatypeName) {
        String xsdStr = createXsd(datatypeName);
        datatypeCreator.generateDatatype(datatypeName, xsdStr);
    }
}
