package org.nrg.xnat.utils;

import com.google.common.io.CharStreams;
import org.nrg.xft.utils.*;
import org.nrg.xft.utils.FileUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 * Created by michael on 6/6/16.
 */
public class DatatypeCreator {

    //String cache = XDAT.getSiteConfigPreferences().getCachePath();
    private String cache = "/tmp/";
    private String buildSpace = "/tmp/plugin-build/";
    private String fileName = "ndar-generated-datatype.xsd";
    private String schemaDir = "src/main/resources/schemas/";

    void generateDatatype(String datatypeName, String xsdStr) {
        this.writeXsdToCache(xsdStr);
        this.setupGradleFiles(datatypeName);
        this.runGradleBuild();
//        this.cleanUp();
    }

    private void writeXsdToCache(String xsdStr) {
        File xsdCacheFile = new File(this.cache + this.fileName);
        Boolean created = xsdCacheFile.getParentFile().mkdirs();
        writeStringToFile(xsdStr, xsdCacheFile);
    }

    private void setupGradleFiles(String datatypeName) {
        File buildDir = new File(this.buildSpace);
        Boolean created = buildDir.mkdir();

        // Gradle skeleton contents into build space
        File src1 = new File(this.cache + "gradle-skeleton");
        File dest1 = new File(this.buildSpace);

        // Generated XSD into gradle project
        File src2 = new File(this.cache + this.fileName);
        File dest2 = new File(this.buildSpace + this.schemaDir + datatypeName + "/" + this.fileName);

        try {
            FileUtils.CopyDir(src1, dest1, true);
            FileUtils.CopyFile(src2, dest2, true);
        } catch (IOException e) {
            System.out.println(e);
        }

        String settingsStr = "rootProject.name = '" + datatypeName + "-schema-plugin'";

        File xsdCacheFile = new File(buildSpace + "settings.gradle");
        created = xsdCacheFile.getParentFile().mkdirs();

        writeStringToFile(settingsStr, xsdCacheFile);
    }

    private void runGradleBuild() {
        File gradlew = new File(this.buildSpace + "gradlew");
        gradlew.setExecutable(true, false);
        Process p = null;

        try {
            //p = new ProcessBuilder(this.buildSpace + "gradlew", "jar").start();
            p = Runtime.getRuntime().exec(new String[] {"bash", "-c", this.buildSpace+"gradlew", "jar"});
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            String in = CharStreams.toString(new InputStreamReader(p.getInputStream()));
            System.out.println(in);
            String err = CharStreams.toString(new InputStreamReader(p.getErrorStream()));
            System.out.println(err);
        } catch (IOException e) {
            System.out.println(e);
        }

//        p.getInputStream();
//        p.getOutputStream();
//        p.getErrorStream();
    }

    public void deployPlugin() {
        // Copy generated plugin to xnat.home
        String xnatHome = System.getProperty("xnat.home");
        File pluginDir = new File(xnatHome + "/plugin");
        File pluginJar = new File("build/lib/*.jar");
    }

    private void writeStringToFile(String string, File file) {
        try {
            PrintWriter printWriter = new PrintWriter(file);
            printWriter.println(string);
            printWriter.close();
        } catch (FileNotFoundException e) {
            System.out.println(e);
        }
    }

    private void cleanUp() {
        File tempXsd = new File(this.fileName);
        File buildSpace = new File(this.buildSpace);
        FileUtils.deleteQuietly(tempXsd);
        FileUtils.deleteDirQuietly(buildSpace);
    }
}