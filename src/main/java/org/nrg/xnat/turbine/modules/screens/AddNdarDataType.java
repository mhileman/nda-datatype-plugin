/*
 * org.nrg.xnat.turbine.modules.screens.AddNdarDataType
 * XNAT http://www.xnat.org
 * Copyright (c) 2015, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.turbine.modules.screens;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.screens.AdminScreen;
import org.nrg.xnat.utils.NdarDatatypeImportUtils;

import java.util.Map;
import java.util.ArrayList;


public class AddNdarDataType extends AdminScreen {

    static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AddNdarDataType.class);

    protected void doBuildTemplate(RunData data, Context context) throws Exception {
        NdarDatatypeImportUtils ndarUtil = new NdarDatatypeImportUtils();
        ArrayList<Map<String, Object>> dataTypes = ndarUtil.getJsonList(ndarUtil.apiUrl + "datastructure");
        context.put("ndarDataTypes", dataTypes);
    }
}
