package org.nrg.xnat.restlet.extensions;

/**
 * Created by Michael Hileman on 2015/12/29.
 * Service to create XSD for a given NDAR data type.
 * For more info on NDAR, see https://ndar.nih.gov/ndar_data_dictionary.html
 */

import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.utils.NdarDatatypeImportUtils;
import org.nrg.xnat.utils.DatatypeCreator;

import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;

import org.apache.log4j.Logger;
import org.restlet.data.Status;

@XnatRestlet({"/services/ndar-import"})
public class NdarDatatypeImportRestlet extends SecureResource {
    static private Logger logger = Logger.getLogger(org.nrg.xnat.restlet.extensions.NdarDatatypeImportRestlet.class);

    public NdarDatatypeImportRestlet(final Context context, final Request request, final Response response) {
        super(context, request, response);
    }

    @Override
    public boolean allowGet() { return true; }

    @Override
    public boolean allowPost() { return true; }

    @Override
    public boolean allowPut() { return false; }

    @Override
    public boolean allowDelete() { return false; }

    @Override
    public void handleGet() {
        final StringBuilder sb = new StringBuilder("<html>");
        sb.append("<h2>NDAR Data Type Importer</h2>");
        sb.append("<h3>query parameters:</h3>");
        sb.append("<p>datatype</p>");
        sb.append("</html>");
        this.getResponse().setEntity(sb.toString(), MediaType.TEXT_HTML);
    }

    @Override
    public void handlePost() {
        logger.info("POSTing");
        String ndarDataType = this.getQueryVariable("datatype");
        String deploy = this.getQueryVariable("deploy");
        NdarDatatypeImportUtils ndarUtil = new NdarDatatypeImportUtils();

        if (ndarDataType != null) {
            ndarUtil.generateDatatype(ndarDataType);
//            if (xsd != null) {
//                this.returnString(xsd, Status.SUCCESS_OK);
//            }
        } else {
            this.returnString("You must specify a 'datatype' param", Status.SUCCESS_OK);
            return;
        }

        if (deploy.equals("true")) {
            ndarUtil.datatypeCreator.deployPlugin();
        }
    }
}

